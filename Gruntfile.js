module.exports = function (grunt) {


    // Configurable paths for the application
    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'dist'
    };


    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');


    grunt.initConfig({
            yeoman: appConfig,

            connect: {
                options: {
                    port: 9000,
                    // Change this to '0.0.0.0' to access the server from outside.
                    hostname: 'localhost',
                    livereload: 35729
                }
            },

            mochaTest: {
                test: {
                    options: {
                        reporter: 'spec',
                        captureFile: 'test_results.txt', // Optionally capture the reporter output to a file
                        quiet: false, // Optionally suppress output to standard out (defaults to false)
                        clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
                    },
                    src: ['test/**/*.js']
                }
            },

            // Make sure there are no obvious mistakes
            jshint: {
                options: {
                    jshintrc: 'jshintrc',
                    reporter: require('jshint-stylish'),
                    force: true
                },
                all: {
                    src: [
                        'Gruntfile.js',
                        'src/models/**/*.js',
                        'src/lib/**/*.js',
                        'src/public/javascripts/**/*.js',
                        'src/routes/**/*.js',
                        'src/config.js',
                        'src/ratify.js',
                        'test/**/*.js'
                    ]
                }
            },


            wiredep: {

                task: {

                    // Point to the files that should be updated when
                    // you run `grunt wiredep`
                    src: [
                        "src/public/imports/frontend_import.html"
                    ],

                    options: {
                        // See wiredep's configuration documentation for the options
                        // you may pass:

                        // https://github.com/taptapship/wiredep#configuration
                    }
                }
            },


            // Copies remaining files to places other tasks can use
            copy: {
                dist: {
                    files: [{
                        expand: true,
                        dot: true,
                        cwd: 'src',
                        dest: appConfig.dist,
                        src: [
                            '**/*.{js,html,hjs,css}',
                            '**/*.{svg,jpg,woff2,woff,ttf}'
                        ]
                    }]

                }
            },

            clean: ["dist/"],

            concat: {}


        }
    );

    grunt.registerTask('build', [

        'wiredep',
        'clean',
        'copy'
    ]);


    grunt.registerTask('default', [
        'jshint',
        'mochaTest',
        'build'
    ]);


};