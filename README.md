# RATIFY #

Ratify is a tool for evaluating playlists using custom evaluation criteria. 
Based on the [Deezer API](https://developers.deezer.com/api), the tool provides an interface similar
to popular streaming websites that allows the user to rate certain playlist parameters with access to the content.

The goal is to make it as easy as possible for a researcher to perform user studies on custom playlist generation
schemes, without having to bother about rights issues and enabling the evaluation at home, 
hopefully lowering the bar for potential test persons.

This project was created as part of a *media project* at the [Institute of Media Technology](imt)
at [Ilmenau University of Technology](https://www.tu-ilmenau.de/).


[imt]:https://www.tu-ilmenau.de/en/institute-for-media-technology/



### Prerequisites ###

#### Node & NPM

The current version was tested with `Node v4.4.3` and `npm 2.15.1`. Older and newer versions will probably work as well.
For fetching client side packages, `bower 1.7.9` is used.
 
####  MongoDB

*Ratify* uses [MongoDB](https://mongodb.com/) as backend storage (tested with `v2.4` and `v3.2`),
please configure the the database connection in `src/config.js`.

#### Web application

The application works best in browser using the chromium engine. Flash must be enabled.


### Install & Start

Clone this repository and run the following commands. Note: `npm test` checks the database connection.

    npm install
    bower install
    npm test
    npm start
       
Per default (see `src/config.js`), *Ratify* runs on http://localhost:3000.


## Getting started ##

# Add first user

Run `node addAdminUser.js <username> <password>`
* **TODO**: add first playlist


## Contributors

* Stephan Fremerey (TU Ilmenau)
* Thomas Köllmer (TU Ilmenau)
* Anna Linke (TU Ilmenau)
* Julian Zebelein (TU Ilmenau)



## License

MIT License - https://opensource.org/licenses/MIT

