/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */



var paramCount = process.argv.length -2;


if (paramCount != 2) {
    console.error("Usage: node addAdminUser.js USERNAME PASSWORD");
    process.exit(-1);
} else {
    var username = process.argv[2];
    var password = process.argv[3];

    console.log(username);
    console.log(password);

    var config = require("./config");
    var mongoose = require('mongoose');

    //connection to MongoDB

    var playlist_db = config.db["production"];
    mongoose.connect(playlist_db);
    var db = mongoose.connection;
    db.on('error', function (err) {
        console.error('Failed connecting to MongoDB.', err);
        process.exit(-1);
    });
    db.once('open', function () {
        console.log('Database connected: %s', playlist_db);
    });


    require('./models/users');
    var User = mongoose.model('user');



    // create the user
    var newUser = new User();

    // set the user's local credentials
    newUser.name = username;
    newUser.password = newUser.generateHash(password);

    // save the user
    newUser.save(function (err) {
        if (err) {
            console.error(err);
            process.exit(-1);
        }

        console.log("User " + username + " created.");
        process.exit();

    });






    
}
