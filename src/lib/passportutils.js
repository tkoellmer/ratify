/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


module.exports = {
    initializePassport: function (passport, mongoose) {



        var LocalStrategy = require('passport-local').Strategy;
        var User = mongoose.model('user');

        /*
         Usage of Passport.
         */

        //passport session setup
        passport.serializeUser(function (user, done) {
            done(null, user);
        });

        passport.deserializeUser(function (id, done) {
            User.findById(id, function (err, user) {
                done(err, user);
            });
        });

        //SIGN IN passport local strategy
        passport.use('local-signin', new LocalStrategy(
            function (username, password, done) {
                User.findOne({
                    name: username
                }, function (err, user) {
                    if (err) {
                        return done(err);
                    }

                    //if no user found
                    if (!user) {
                        return done(null, false, {
                            message: 'Incorrect username.'
                        });
                    }

                    //if user found, but wrong password
                    if (!user.validPassword(password)) {
                        return done(null, false, {
                            message: 'Incorrect password.'
                        });
                    }

                    //user found, right password
                    return done(null, user);
                });
            }
        ));

        //SIGN UP passport local strategy
        passport.use('local-signup', new LocalStrategy(
            function (username, password, done) {

                process.nextTick(function () {

                    // checking if the user trying to sign up already exists
                    User.findOne({
                        name: username
                    }, function (err, user) {
                        if (err) {
                            return done(err);
                        }

                        // check to see if there is already a user with that username
                        if (user) {
                            return done(null, false, {
                                message: 'Username is already taken.'
                            });
                        } else {

                            // create the user
                            var newUser = new User();

                            // set the user's local credentials
                            newUser.name = username;
                            newUser.password = newUser.generateHash(password);

                            // save the user
                            newUser.save(function (err) {
                                if (err) {
                                    throw err;
                                }
                                return done(null, newUser);
                            });
                        }
                    });
                });
            }));


    }
};
