
/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */

var utils = require("./utils.js");


module.exports = {

    initializeSockets: function (mongoose, io) {


        // Load Database models
        var Song = mongoose.model('songs');
        var Playlist = mongoose.model('playlists');
        var Feedback = mongoose.model('feedbacks');


        // Write Playlist to Songs in MongoDB
        io.on('connection', function (socket) {
            socket.on('savePlaylistToDB', function (playlistTitle, trackList, heartRatingEnabled, starRatingEnabled, thumbUpRatingEnabled, sunRatingEnabled, happyRatingEnabled, overallRatingEnabled) {
                Song.find({
                    'playlistTitle': playlistTitle
                }, function (err, data) {
                    if (err) {
                        return new Error('No songs found.');
                    } else {
                        if (data == "") {
                            for (i = 0; i < trackList.length; i++) {
                                // Add tracks to Songlist
                                var duration = utils.toHHMMSS(trackList[i].duration);
                                var newSong = new Song({
                                    title: trackList[i].title,
                                    artist: trackList[i].artist.name,
                                    duration: duration,
                                    trackId: trackList[i].id,
                                    playlistTitle: playlistTitle,
                                    positionInPlaylist: i + 1,
                                    colorTag: "None"
                                });
                                newSong.save(); // Save song to Database.
                            }
                            Song.find({
                                'playlistTitle': playlistTitle
                            }, function (err, data) {
                                if (err) {
                                    return new Error('No songs found.');
                                } else {
                                    var newPlaylist = new Playlist({
                                        playlistId: playlistTitle,
                                        heartRatingEnabled: heartRatingEnabled,
                                        starRatingEnabled: starRatingEnabled,
                                        thumbUpRatingEnabled: thumbUpRatingEnabled,
                                        sunRatingEnabled: sunRatingEnabled,
                                        happyRatingEnabled: happyRatingEnabled,
                                        overallRatingEnabled: overallRatingEnabled,
                                        songs: data
                                    });
                                    newPlaylist.save(); // Save the playlist to database
                                }
                            });
                            socket.emit('playlistSuccessfullySavedToDB');
                        } else {
                            socket.emit('playlistExistsAlready');
                        }
                    }
                });
            });
        });


// Write Song with it's rating to MongoDB.
        io.on('connection', function (socket) {
            socket.on('rateSong', function (ratingLevel, songId, ratingType) {
                Song.findById(songId, function (err, song) {
                    if (err || !song) {
                        return new Error('Not Found.');
                    } else {
                        var sessionfound = false;
                        var allratings = new Array();
                        var length = song[ratingType].length;
                        for (var i = 0; i < song[ratingType].length; i++) {
                            if (song[ratingType][i].sessionId == sessionId) {
                                song[ratingType][i].rating = ratingLevel; //replace rating if session exists
                                sessionfound = true;
                                song.save();
                            }
                        }
                        if (sessionfound == false) {
                            for (var y = 0; y <= length; y++) {
                                if (y < song[ratingType].length) {
                                    allratings.push(song[ratingType][y]); //write all existing ratings into allratings-array
                                }
                                if (y == length) {
                                    allratings.push({
                                        rating: ratingLevel,
                                        sessionId: sessionId
                                    }); //add new rating to allratings-array
                                    // Save the rating level to it's related rating type in songs-collection via JSON bracket notation.
                                    song[ratingType] = allratings; //save ratings to song
                                    song.save();
                                }
                            }
                        }
                    }
                });
            });
            socket.on('ratePlaylist', function (ratingLevel, playlistId, ratingType) {
                Playlist.findById(playlistId, function (err, playlist) {
                    if (err || !playlist) {
                        return new Error('Not Found.');
                    } else {
                        var sessionfound = false;
                        var allratings = new Array();
                        var length = playlist[ratingType].length;
                        for (var i = 0; i < playlist[ratingType].length; i++) {
                            if (playlist[ratingType][i].sessionId == sessionId) {
                                playlist[ratingType][i].rating = ratingLevel; //replace rating if session exists
                                sessionfound = true;
                                playlist.save();
                            }
                        }
                        if (sessionfound == false) {
                            for (var y = 0; y <= length; y++) {
                                if (y < playlist[ratingType].length) {
                                    allratings.push(playlist[ratingType][y]); //write all existing ratings into allratings-array
                                }
                                if (y == length) {
                                    allratings.push({
                                        rating: ratingLevel,
                                        sessionId: sessionId
                                    }); //add new rating to allratings-array
                                    // Save the rating level to it's related rating type in playlist-collection via JSON bracket notation.
                                    playlist[ratingType] = allratings; //save ratings to playlist
                                    playlist.save();
                                }
                            }
                        }
                    }
                });
            });
            socket.on('submitRatifyFeedback', function (ratifyFeedback) {
                Feedback.find(function (err, feedback) {
                    if (err || !feedback) {
                        return new Error('Not Found.');
                    } else {
                        var newRatifyFeedback = new Feedback({
                            ratifyFeedback: [{
                                rating: ratifyFeedback,
                                sessionId: sessionId
                            }]
                        });
                        newRatifyFeedback.save();
                    }
                });
                socket.emit('thankYouForFeedback');
            });
        });

// Delete a Playlist with all songs containing.
        io.on('connection', function (socket) {
            socket.on('deletePlaylist', function (playlistToDelete) {
                // Find the playlist by its unique ID.
                Playlist.findById(playlistToDelete, function (err, playlist) {

                    if (err || !playlist) {
                        socket.emit('playlistAlreadyDeleted');
                        console.log('Playlist ID ' + playlistToDelete + ' already deleted.');
                    } else {
                        // Delete every song the playlist contains.
                        var ratifyPlaylistName = playlist.playlistId;
                        for (var i = 0; i < playlist.songs.length; i++) {
                            console.log("Deleting Song " + playlist.songs[i] + "...");
                            Song.findById(playlist.songs[i], function (err, song) {
                                if (err || !song) {
                                    return new Error('Song ID ' + song._id + ' not found.');
                                } else {
                                    song.remove(function (err) {
                                        if (err) {
                                            return new Error('Something went wrong here.');
                                        } else {
                                            console.log('Song with ID ' + song._id + ' deleted');
                                        }
                                    });
                                }
                            })
                        }
                        // Remove the playlist.
                        playlist.remove(function (err) {
                            if (err) {
                                return new Error('Something went wrong.');
                            } else {
                                console.log('Playlist ID ' + playlist._id + ' deleted');
                                console.log('Ratify ID ' + ratifyPlaylistName);
                                socket.emit('playlistDeleted', ratifyPlaylistName);
                            }
                        });
                    }
                });
            });
        });

    }
};