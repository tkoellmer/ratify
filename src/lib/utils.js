/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


module.exports = {

    setErrorHandlers: function (app) {


        /*
         Error handlers and starting Server.
         */

        // catch 404 and forward to error handler
        app.use(function (req, res, next) {
            var err = new Error('Not Found');
            err.status = 404;
            next(err);
        });


        // development error handler
        // will print stacktrace
        if (app.get('env') === 'development') {
            app.use(function (err, req, res, next) {
                res.status(err.status || 500);
                res.render('error', {
                    message: err.message,
                    error: err
                });
            });
        }

        // production error handler
        // no stacktraces leaked to user
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.render('error_nostacktrace', {
                message: err.message,
                error: {}
            });
        });
    },

    prepare_song_array: function (data) {
        /**
         * create one array with all songnames and ids that will be displayed in the table
         * @type {Array}
         */

        var allsongs = [];

        for (var i = 0; i < data.length; i++) {
            allsongs.push({
                table_title: data[i].title,
                table_artist: data[i].artist,
                table_duration: data[i].duration,
                table_trackId: data[i].trackId,
                table_playlistId: data[i].playlistTitle,
                table_positionInPlaylist: data[i].positionInPlaylist,
                table_objectId: data[i]._id,
                table_rating: data[i].rating,
                table_colorTag: data[i].colorTag
            });
        }
        return allsongs;
    },

    // Util-Function to convert the input of seconds to an hh:mm:ss format.
    // See https://stackoverflow.com/questions/6312993/javascript-seconds-to-time-string-with-format-hhmmss
    toHHMMSS: function (duration) {
        var sec_num = parseInt(duration, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        var hourSeparator = ':';
        var minuteSeparator = ':';

        if (hours == 0) {
            hours = '';
            hourSeparator = '';
        }
        if (minutes < 10 && hours != 0) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        return hours + hourSeparator + minutes + minuteSeparator + seconds;
    }

};