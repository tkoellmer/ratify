/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */

/* This model defines the data type "Feedback" */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//define Schema for feedback
var feedbackSchema = new Schema({
  ratifyFeedback: [{
    rating: String,
    sessionId: String
  }]
}, {
  collection: 'feedbacks'
}); //all feedbacks will be saved to the collection 'feedbacks'

var Feedback = mongoose.model('feedbacks', feedbackSchema);

module.exports = Feedback;
