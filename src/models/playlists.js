/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


/* This model defines the data type "Playlist" */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Song = require('./songs');

//define Schema for playlist
var playlistSchema = new Schema({
  playlistId: String,
  heartRatingEnabled: Boolean,
  starRatingEnabled: Boolean,
  thumbUpRatingEnabled: Boolean,
  sunRatingEnabled: Boolean,
  happyRatingEnabled: Boolean,
  overallRatingEnabled: Boolean,
  overallRating: [{
    rating: String,
    sessionId: String
  }],
  songs: [{
    type: Schema.Types.ObjectId,
    ref: 'songs'
  }]
}, {
  collection: 'playlists'
}); //all playlists will be saved to the collection 'playlists'

var Playlist = mongoose.model('playlists', playlistSchema);

module.exports = Playlist;
