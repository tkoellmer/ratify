/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


// Function to search for a single song via the Deezer API and adding it to the playlist.
function addSingleSongToPlaylist(trackTitle, trackArtist, trackAlbum) {
    DZ.api('/search?q=track:"' + trackTitle + '" artist:"' + trackArtist + '" album="' + trackAlbum + '"', function (response) {
        if (response && !response.error) {
            var loadedTracks = getLoadedTracks();
            // Adding a song to the player's queuue works only if already one song is loaded.
            if (loadedTracks.length == 0) {
                // Add the first song from Deezer API call to the playlist.
                // TODO: check for empty response
                playerPlayTracks([response.data[0].id]);
            } else {
                playerAddSongToQueque([response.data[0].id]);
            }
            var scope = angular.element(document.getElementById('playlistTable')).scope();
            scope.$apply(function () {
                var actualTitleDuration = parseInt(response.data[0].duration);
                scope.addSongToPlaylistTable(scope.playlist.length + 1, response.data[0].title, toHHMMSS(actualTitleDuration), response.data[0].artist.name);
            });
        }
    });

}

// Function to delete a playlist via the socket.
function deletePlaylistViaSocket() {
    var socket = io();
    var scope = angular.element(document.getElementById('playlistTable')).scope();
    var playlistToDelete = scope.getSelectedPlaylistID();
    socket.emit('deletePlaylist', playlistToDelete);
    socket.on('playlistDeleted', function (ratifyPlaylistName) {
        $('#playlistDeletedAlert').attr('class', 'alert alert-success');
        $('#playlistDeletedAlert').text("Successfully deleted the playlist test '" + ratifyPlaylistName + "' from Ratify.");
    });
    socket.on('playlistAlreadyDeleted', function () {
        $('#playlistDeletedAlert').attr('class', 'alert alert-warning');
        $('#playlistDeletedAlert').text("The playlist you wanted to delete is already deleted from Ratify!");
    });
    scope.reloadDisplayedPlaylists();
}

// Function to import a playlist via JSON-file.
function importPlaylistViaJSON() {
    var uploadedFile = document.querySelector('input[type=file]').files[0];
    var scope = angular.element(document.getElementById('playlistTable')).scope();
    if (uploadedFile) {
        var r = new FileReader();
        r.readAsText(uploadedFile);
        r.onload = function (e) {
            var importedPlaylist = e.target.result;
            // Parse the imported text to JSON.
            var importedPlaylistJSON = JSON.parse(importedPlaylist);
            var i = 0;
            songsNotFound = false;
            var loopThroughs = importedPlaylistJSON.songList.length;
            // Function to add multiple songs to the playlist via Deezer API calls.
            function addMultipleSongsToPlaylist() {
                DZ.api(
                    '/search?q=track:"' + importedPlaylistJSON.songList[i].songTitle
                    + '" artist:"' + importedPlaylistJSON.songList[i].artist
                    + '" album="' + importedPlaylistJSON.songList[i].albumTitle + '"',

                    function (response) {
                        if (response && !response.error) {
                            // If a song couldn't be found, set boolean.
                            if (response.data.length == 0) {
                                console.log("Test");
                                songsNotFound = true;
                            } else {
                                var loadedTracks = getLoadedTracks();
                                scope.$apply(function () {
                                    var actualTitleDuration = parseInt(response.data[0].duration);
                                    scope.addSongToPlaylistTable(scope.playlist.length + 1, response.data[0].title, toHHMMSS(actualTitleDuration), response.data[0].artist.name);
                                    // Push Song ID to array, then load them to the player.
                                    scope.songIDs.push(response.data[0].id);
                                    DZ.player.playTracks(scope.songIDs, false);
                                });
                            }
                            // If import is finished: alert for user if entire playlist could be imported or not.
                            if (i == loopThroughs) {
                                if (!songsNotFound) {
                                    $("#jsonPlaylistImportAlert").attr('class', 'alert alert-success');
                                    $('#jsonPlaylistImportAlert').text("Successfully imported all songs from the playlist. Please save the playlist below to finish the prodecure.");
                                } else {
                                    $("#jsonPlaylistImportAlert").attr('class', 'alert alert-warning');
                                    $('#jsonPlaylistImportAlert').text("One or more songs could not be imported. Maybe they are not available on Deezer or you have to specify the song's metadata.");
                                }
                            }
                        }
                    });
                // Call the function itself again until every song from JSON is processed.
                // Need 1 second break here, because Deezer API calls need time to answer.
                // (Otherwise playlist order would not be the same as in the JSON-file.)
                i++;
                if (i < loopThroughs) {
                    setTimeout(addMultipleSongsToPlaylist, 1000);
                }
            }

            addMultipleSongsToPlaylist();
            // Adding a song to the player's queque works only if already one song is loaded.
            $("#playlistTitle").val(importedPlaylistJSON.playlistTitle);
        }
    }
}

// Function to submit the player's loaded playlist to the socket.
function submitPlaylistToSocket() {
    var socket = io();
    var trackList = getLoadedTracks();
    var playlistTitle = $("#playlistTitle").val();
    var heartRatingEnabled = $('#heartRatingCheckbox').attr('aria-checked');
    var starRatingEnabled = $('#starRatingCheckbox').attr('aria-checked');
    var ratingThreeEnabled = $('#ratingThreeCheckbox').attr('aria-checked');
    var ratingFourEnabled = $('#ratingFourCheckbox').attr('aria-checked');
    var ratingFiveEnabled = $('#ratingFiveCheckbox').attr('aria-checked');
    var overallRatingEnabled = $('#overallRatingCheckbox').attr('aria-checked');
    var scope = angular.element(document.getElementById('playlistTable')).scope();
    $("#submitPlaylistToDB").submit(function () {
        socket.emit('savePlaylistToDB', playlistTitle, trackList, heartRatingEnabled, starRatingEnabled, ratingThreeEnabled, ratingFourEnabled, ratingFiveEnabled, overallRatingEnabled);
        socket.on('playlistSuccessfullySavedToDB', function () {
            $('#playlistExistsAlert').attr('class', 'alert alert-success');
            $('#playlistExistsAlert').text("Successfully added the playlist test '" + playlistTitle + "' to Ratify.");
            $('#submitPlaylistToDB span').attr('class', 'glyphicon glyphicon-ok');
        });
        socket.on('playlistExistsAlready', function () {
            $('#playlistExistsAlert').attr('class', 'alert alert-danger');
            $('#playlistExistsAlert').text("This playlist test already exists. Please choose an other name for it!");
        });
        return false;
    });
    scope.resetPlaylistTable();
    scope.resetSongIDsArray();
}
