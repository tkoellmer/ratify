/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


// Get the cover of an album by calling the Deezer API.
function getAlbumCoverViaDeezerAPI(albumID, currentAlbumCoverElement, size) {
  DZ.api('/album/' + albumID, function(response) {
    var cover = response.cover + '&size=' + size;
    $(currentAlbumCoverElement).attr('src', cover);
  });
}

// Function to get all loaded tracks inside the player within an array.
function getLoadedTracks() {
  var trackList = DZ.player.getTrackList();
  return trackList;
}

// Get the duration of the loaded Playlist.
function getPlaylistDuration() {
  var trackList = DZ.player.getTrackList();
  var duration = 0;
  for (i = 0; i < trackList.length; i++) {
    duration = duration + parseInt(trackList[i].duration);
  }
  duration = toHHMMSS(duration);
  $('#miscellaneous').text('Duration: ' + duration);
}

/*
Function to initialize Event handlers:
Show track's (Album) Title, time, adjust Bar to actual position of track.
*/
function initializeEventHandlers() {
  var trackTimeGone = toHHMMSS("0");
  DZ.Event.subscribe('current_track', function(arg) {
    var trackAlbumTitleElement = document.getElementById('trackAlbumTitle');
    var trackTimeElement = document.getElementById('trackTime');
    var trackDuration = toHHMMSS(arg.track.duration);
    trackTimeElement.innerHTML = ' / ' + trackDuration;
    trackAlbumTitleElement.innerHTML = '<b>' + arg.track.title + '</b> by <i>' + arg.track.artist.name + '</i>';
    getAlbumCoverViaDeezerAPI(arg.track.album.id, '#currentAlbumCover', 'small');
  });
  DZ.Event.subscribe('player_position', function(arg) {
    var trackTimeGoneElement = document.getElementById('trackTimeGone');
    trackTimeGone = toHHMMSS(arg[0]);
    trackTimeGoneElement.innerHTML = trackTimeGone;
    $("#playerPositionSlider").find('.bar').css('width', (100 * arg[0] / arg[1]) + '%');
  });
  DZ.Event.subscribe('volume_changed', function(arg) {
    if (arg === 0) {
      document.getElementById("playerMuteSpan").className = "glyphicon glyphicon-volume-off";
    } else {
      document.getElementById("playerMuteSpan").className = "glyphicon glyphicon-volume-up";
    }
    document.getElementById("playerVolumeSlider").value = arg;
  });
}

// Login to the Deezer account of the user.
function loginToDeezer() {
  DZ.login(function(response) {
    if (response.authResponse) {
      console.log('Welcome! Fetching your information...');
      DZ.api('/user/me', function(response) {
        console.log('Good to see you, ' + response.name + '.');
      });
    } else {
      console.log('User cancelled login or did not fully authorize.');
    }
  }, {
    perms: 'basic_access,email'
  });
}

// Things to get done when player loaded.
function onPlayerLoaded() {
  // Make Player-buttons and dropdown-box for playlist election interactable.
  $("#controlers button").attr('disabled', false);
  $("#playlistAngular select").attr('disabled', false);
  // For Admin-Page: Make Buttons for creating and saving the playlist interactable.
  $("#loadToPlayerButton").attr('disabled', false);
  $("#importFromJSONButton").attr('disabled', false);
  $("#addSongToPlaylistButton").attr('disabled', false);
  $("#savePlaylistToRatifyButton").attr('disabled', false);

  $(".playlistItem").on("click", function() {
      console.log("Click!");
      console.log(DZ.player.getCurrentIndex());

      var trackList = DZ.player.getTrackList();
      DZ.player.playTracks(trackList, 2);



  });

}

// Function to add a single song to the player's queque.
function playerAddSongToQueque([songId]) {
  DZ.player.addToQueue([songId]);
}

// Function to load a Ratify playlist to the player and show it in the regarding table.
// Input: Ratify playlist ID.
function playerLoadRatifyPlaylist(selectedPlaylistId) {
  var scope = angular.element(document.getElementById('playlistAngular')).scope();
  var allsongs = scope.getAllsongs();
  var songIds = [];
  for (var i = 0; i < allsongs.data.length; i++) {
    if (String(allsongs.data[i].table_playlistId) == selectedPlaylistId) {
      songIds.push(allsongs.data[i].table_trackId);
    }
  }
  DZ.player.playTracks(songIds, false);
}

// Function to load a Deezer playlist to the player and show it in the regarding table.
function playerLoadDeezerPlaylist() {
  var playlistID = parseInt(document.getElementById("deezerPlaylistID").value, 10);
  DZ.player.playPlaylist(playlistID, false);
  // Wait 500 ms because the player needs a moment for loading the playlist.
  setTimeout(function() {
    var loadedPlaylist = getLoadedTracks();
    var scope = angular.element(document.getElementById('playlistTable')).scope();
    scope.$apply(function() {
      // Clear the playlist so that a playlist couldn't appear twice in the table
      scope.resetPlaylistTable();
      for (i = 0; i < loadedPlaylist.length; i++) {
        var actualTitleDuration = parseInt(loadedPlaylist[i].duration);
        scope.addSongToPlaylistTable(i + 1, loadedPlaylist[i].title, toHHMMSS(actualTitleDuration), loadedPlaylist[i].artist.name);
      }
    })
  }, 500);
}

// Function to set the player's volume.
function playerMuteUnmute() {
  var preVolume = document.getElementById("preVolume").value;
  document.getElementById("preVolume").value = DZ.player.getVolume();
  if (DZ.player.getVolume() != 0) {
    DZ.player.setVolume(0);
    document.getElementById("playerMuteSpan").className = "glyphicon glyphicon-volume-off";
  } else {
    DZ.player.setVolume(preVolume);
    document.getElementById("playerMuteSpan").className = "glyphicon glyphicon-volume-up";
  }
}

// Function to control the Player via one Button for "Play" and "Pause"-Command
function playerPlayPause() {
  DZ.Event.subscribe('player_paused', function(arg) {
    document.getElementById("playPauseSpan").className = "glyphicon glyphicon glyphicon-play";
  });
  DZ.Event.subscribe('player_play', function(arg) {
    document.getElementById("playPauseSpan").className = "glyphicon glyphicon glyphicon-pause";
  });
  if (DZ.player.isPlaying()) {
    DZ.player.pause();
    document.getElementById("playPauseSpan").className = "glyphicon glyphicon glyphicon-play";
  }
  if (DZ.player.isPlaying() === false) {
    DZ.player.play();
    document.getElementById("playPauseSpan").className = "glyphicon glyphicon glyphicon-pause";
  }
}

// Function to play tracks with the player.
function playerPlayTracks([trackId]) {
  DZ.player.playTracks([trackId], false);
}

$(document).ready(function() {
  $("#playerPositionSlider").click(function(evt, arg) {
    DZ.player.seek((evt.offsetX / $(this).width()) * 100);
  });
  $("#playerVolumeSlider").click(function(evt, arg) {
    DZ.player.setVolume((evt.offsetX / $(this).width()) * 100);
  });
})
