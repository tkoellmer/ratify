/* initialise Deezer player */

DZ.init({
  appId: '170751',
  channelUrl: 'http://localhost:3000',
  player: {
    onload: function() {
      onPlayerLoaded();
      initializeEventHandlers();
    }
  }
});
