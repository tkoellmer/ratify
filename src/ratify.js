/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */

//setup
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http').Server(app);
var io = require('socket.io')(http);

var config = require('./config');
var ioUtils = require('./lib/socketioutils');
var passportUtils = require('./lib/passportutils');
var ratifyUtils = require("./lib/utils.js");


GLOBAL.sessionId = 0; //global variable SessionID



//connection to MongoDB
var playlist_db = config.db["production"];
mongoose.connect(playlist_db);

var db = mongoose.connection;
db.on('error', function (err) {
    console.error('Failed connecting to MongoDB.', err);
    process.exit(-1);
});
db.once('open', function () {
    console.log('Database connected: %s', playlist_db);
});



// Express Setup
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));



// Set HoganJS as viewing engine and change its Delimiters as AngularJS uses the same ones.
// TODO: is it necessary to use BOTH?
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');
app.locals.delimiters = '{% %}';



//Passport setup
app.use(session({
    secret: 'sedrfgvhbknjm',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());



app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));



//load models
require('./models/users');
require('./models/songs');
require('./models/playlists');
require('./models/feedbacks');

//load routes
var routes = require('./routes/index');
var users = require('./routes/users');
var signin = require('./routes/signin');
var admin = require('./routes/admin');
var signup = require('./routes/signup');
var about = require('./routes/about');
var impressum = require('./routes/impressum');
var feedback = require('./routes/feedback');

//define routes
app.use('/', routes);
app.use('/users', users);
app.use('/signin', signin);
app.use('/admin', admin);
app.use('/signup', signup);
app.use('/about', about);
app.use('/impressum', impressum);
app.use('/feedback', feedback);



ioUtils.initializeSockets(mongoose, io);
passportUtils.initializePassport(passport, mongoose);
ratifyUtils.setErrorHandlers(app);



//starting server
var port = config.server_config["port"];
http.listen(port, function () {
    console.log('listening on *:' + port);
});



module.exports = app;
