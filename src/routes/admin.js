/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');
var fs = require('fs');
var Song = mongoose.model('songs');
var Playlist = mongoose.model('playlists');

/* GET admin page. */
router.get('/', isLoggedIn, function(req, res, next) {
  res.render('admin', {
    title: 'Admin',
    text: 'You are logged in as admin',
    username: req.session.passport.user.name
  });
});

router.get('/export', function(req,res){
    exportAllData();
    var file = 'public/export.json';
    res.download(file);
});

router.get('/dologout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

//route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated()){
        return next();
    }

    // if user is not authenticated, redirect to the sign-in page
    res.redirect('/signin');
}

//export all songs and playlists to JSON
function exportAllData(){
    var jsonOutputSongs;
    var jsonOutput;
    Song.find().exec(function(err, output){
        jsonOutputSongs = output;
    });

    Playlist.find().exec(function(err, output){

        jsonOutput = JSON.stringify(jsonOutputSongs) + JSON.stringify(output);
        console.log(jsonOutput);

        fs.writeFile('public/export.json', jsonOutput, function (err) {
            if (err) {
                console.log(err);
            }
            else{
                console.log('Data exported successfully');
            }
    });
})
}

module.exports = router;