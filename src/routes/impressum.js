/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */

var express = require('express');
var router = express.Router();

/* GET impressum page. */
router.get('/', function(req, res, next) {
  res.render('impressum', {
      title: 'Impressum',
        error: req.flash('error')
    });
});

module.exports = router;
