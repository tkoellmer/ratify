/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET signin page. */
router.get('/', isLoggedOut, function(req, res, next) {
	res.render('signin', {
    	title: 'Sign In',
        error: req.flash('error')
  	});
});

/* Do Login */
router.post('/dologin',
	passport.authenticate('local-signin', {
     	successRedirect : '/admin',
    	failureRedirect : '/signin',
        failureFlash : true
    })
);

//route middleware to make sure a user is logged out
function isLoggedOut(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (!req.isAuthenticated()){
        return next();
    }

    // if user is not authenticated, redirect to the sign-in page
    res.redirect('/admin');
}


module.exports = router;
