/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


var assert = require('assert');

require("./init_test_mongoose");

var Song = require("../src/models/songs");


describe('ModelTest', function() {

  before(function(done) {
    Song.collection.drop(function() {
      console.log("Clearing...");
      done();
    })
  });


  it('should add 5 new song to the db', function() {

    for (var i = 0; i < 5; i++) {


      var testSong = new Song({
        title: "TestTitle",
        artist: "TestArtist",
        trackId: "TestId"
      });

      testSong.save();

    }


  });

  it('retrieves 5 songs', function(done) {

    Song.find({
        "title": "TestTitle"
      },
      function(err, docs) {
        assert(!err);
        assert.equal(docs.length, 5);
        done();
      }
    );


  });

  it("returns array of all 5 songs", function(done) {

    Song.find({
        "title": "TestTitle"
      },
      function(err, docs) {

        var utils = require("../src/lib/utils");

        var song_array = utils.prepare_song_array(docs);
        assert.equal(5, song_array.length);

        done();
      }
    );
  });


});
